package org.jrovira.blog.apm.bankservice.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import co.elastic.apm.api.ElasticApm;
import co.elastic.apm.api.Span;

@RestController
public class AccountProcessController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountProcessController.class);
	
	private static final String TRANSACTION_TAG_NAME = "transaction_tag_example";
	private static final String TRANSACTION_TAG_VALUE = "transaction_tag_example_value";
	private static final String CUSTOM_SPAN_TYPE_VALUE = "type_test";
	private static final String CUSTOM_SPAN_SUBTYPE_VALUE = "subtype_test";
	private static final String CUSTOM_SPAN_ACTION_VALUE = "action_test";
	private static final String CUSTOM_SPAN_NAME = "span_name_test";
	private static final String CUSTOM_SPAN_TAG_NAME = "tag_test";
	private static final String CUSTOM_SPAN_TAG_VALUE = "tag_test_value";
	private static final long SLEEP_TIME = 30;
	
	@Autowired
	private RestTemplate restTemplate;
	@Value("${services.risk.url}")
	private String riskUrl;
	@Value("${services.account.url}")
	private String accountUrl;
	
	@GetMapping("/account")
	public ResponseEntity<String> processAccount(@RequestParam String name, @RequestParam String nif) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Iniciando el proceso de creación de una cuenta para el cliente {} con nif {}.", name, nif);
		}
		addInformationInTransaction();
		otherActions();
		boolean hasRisk = analizeRisk(nif);
		if(hasRisk) {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("No se ha podido crear la cuenta del cliente {} con nif {} por presentar riesgos operacionales.", name, nif);
			}
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		}
		else {
			String accountId = createAccount(name, nif);
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Se ha creado la cuenta {} del cliente {} con nif {} satisfactoriamente.", accountId, name, nif);
			}
			return new ResponseEntity<>(accountId, HttpStatus.OK);
		}
		
	}

	private boolean analizeRisk(String nif) {
		ResponseEntity<Boolean> hasRisk = restTemplate.getForEntity(riskUrl, Boolean.class, nif);
		return hasRisk.getBody();
	}
	
	private String createAccount(String name, String nif) {
		ResponseEntity<String> accountId = restTemplate.postForEntity(accountUrl, null, String.class, nif, name);
		return accountId.getBody();
	}


	private void addInformationInTransaction() {
		ElasticApm.currentTransaction().addTag(TRANSACTION_TAG_NAME, TRANSACTION_TAG_VALUE);
	}

	private void otherActions() {
		Span span = ElasticApm.currentSpan().startSpan(CUSTOM_SPAN_TYPE_VALUE, CUSTOM_SPAN_SUBTYPE_VALUE, CUSTOM_SPAN_ACTION_VALUE);
		span.setName(CUSTOM_SPAN_NAME);
		span.addTag(CUSTOM_SPAN_TAG_NAME, CUSTOM_SPAN_TAG_VALUE);
		try {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Paramos {} ms el proceso.", SLEEP_TIME);
			}
			Thread.sleep(SLEEP_TIME);
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		finally {
			span.end();
		}
	}

}
