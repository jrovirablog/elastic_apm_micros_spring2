package org.jrovira.blog.apm.accountservice.controllers;

import org.jrovira.blog.apm.accountservice.dao.AccountDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

	@Autowired
	private AccountDao accountDao;

	@PostMapping("/account")
	public String createAccount(@RequestParam String name, @RequestParam String nif) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Procedemos a crear una cuenta del cliente {} con nif {}.", name, nif);
		}
		String accountId = accountDao.createAccount(name, nif);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Se ha creado para el cliente {} con nif {} la cuenta con identificador {}.", name, nif, accountId);
		}
		return accountId;
	}
}
