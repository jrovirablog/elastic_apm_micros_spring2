package org.jrovira.blog.apm.accountservice.dao;

public interface AccountDao {

	public String createAccount(String name, String nif);
}
