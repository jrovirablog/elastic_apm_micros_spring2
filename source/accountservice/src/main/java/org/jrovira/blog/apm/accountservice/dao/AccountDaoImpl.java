package org.jrovira.blog.apm.accountservice.dao;

import java.util.UUID;

import org.springframework.stereotype.Component;

import co.elastic.apm.api.CaptureSpan;

@Component
@CaptureSpan(type="database", subtype="mock database")
public class AccountDaoImpl implements AccountDao {

	@Override
	public String createAccount(String name, String nif) {
		return UUID.randomUUID().toString();
	}

}
