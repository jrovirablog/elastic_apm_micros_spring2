package org.jrovira.blog.apm.riskservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RiskserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RiskserviceApplication.class, args);
	}

}
