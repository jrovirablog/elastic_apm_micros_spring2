package org.jrovira.blog.apm.riskservice.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RiskController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RiskController.class);
	
	@GetMapping("/risk/customer")
	public boolean isCustomerOk(@RequestParam String nif) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Analizando el riesgo del cliente {}.", nif);
		}
		boolean risk = Math.random() * 100 % 2 < 1;
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("El cliente {} ha sido catalogado como {}", nif, risk ? "arriesgado" : "seguro");
		}
		return risk;
	}
}
