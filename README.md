# elastic_apm_micros_spring2

ELK 6.6.0
APM Server 6.6.1
APM Agent 1.4.0


1) APM setup (preparar kibana)
2) Descargar APM Agent de maven central https://mvnrepository.com/artifact/co.elastic.apm/elastic-apm-agent/1.4.0 y copiarlo en configuration/apm

Configuration APM

capture_personal_data: true
instrumentation.enabled
instrumentation.environment


Configuration APM Agent

-Delastic.apm.service_name=bankservice
-Delastic.apm.service_version=1.0.0
-Delastic.apm.environment=dev
-Delastic.apm.sanitize_field_names=pass*,pwd
-Delastic.apm.capture_body=all
-Delastic.apm.capture_headers=true
-Delastic.apm.enable_log_correlation=true
-Delastic.apm.application_packages=org.jrovira.blog.apm
-Delastic.apm.server_urls=http://localhost:8200